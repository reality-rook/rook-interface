#!/usr/bin/python3
import sys
sys.path.insert(1, '../_lib')
import page_constructor
import access_management
import discord_actions
import database_handler
import string

page_constructor.headers()
player_id = access_management.check_session()
page_constructor.head('Rook | Edit Game')



import cgi
form = cgi.FieldStorage()
game_id = form.getvalue('edit_game')

if not game_id:
    print('\n\n<meta http-equiv="refresh" content="0; URL=/games/listing"/>')

database_details = database_handler.run('SELECT * FROM game WHERE id=' + str(game_id) + ';').fetchone()
owner_id = database_details[7]

if owner_id != player_id:
    print('\n\n<meta http-equiv="refresh" content="0; URL=/games/listing"/>')

name = database_details[1]
play_type = database_details[3]
running = database_details[4]
discord_role = database_details[5]
discord_channel = database_details[6]
player_tags = []

for player in database_handler.get_players(database_details[0]):
    if player[0] != player_id:
        player_tags.append(database_handler.player_record(player[0])[1])

all_player_tags = []
for tag in database_handler.run('SELECT tag FROM player').fetchall():
    all_player_tags.append(tag[0])
all_player_tags.remove(database_handler.player_record(player_id)[1])

known_channel_roles = discord_actions.run_actions({
    'get_roles': [],
    'get_channels': []
})

known_roles = known_channel_roles['get_roles']
known_channels = known_channel_roles['get_channels']

known_roles["None"] = 0
known_channels["None"] = 0

checks_incorrect = ''
if form.getvalue('update_form'):
    update_sql = 'UPDATE game SET '

    name_error = ''
    if name != form.getvalue('game_name') and form.getvalue('game_name'):
        whitelist = set(string.ascii_lowercase + string.digits + string.ascii_uppercase + ' _-')
        if not any(c not in whitelist for c in name):
            if database_handler.verify_record('game', 'name', form.getvalue('game_name')):
                name_error = name_error + 'That game has already been added to Rook.\n'
        else:
            name_error = name_error + 'Name must be only letters and numbers.\n'
        if name == '':
            name_error = name_error + 'The game must have a name.\n'
        if len(name) > 32:
            name_error = name_error + 'The game name must be less than 33 characters.\n'
        if name_error == '':
            update_sql = update_sql + 'name="' + form.getvalue('game_name') + '", '
            name = form.getvalue('game_name')
    checks_incorrect = checks_incorrect + name_error

    play_type_error = ''
    if play_type != form.getvalue('game_play_type') and form.getvalue('game_play_type'):
        if not (form.getvalue('game_play_type') in ['longform', 'shortform', 'oneshot', 'unknown']):
            play_type_error = play_type_error + 'Uknown game type.\n'
        if play_type_error == '':
            update_sql = update_sql + 'type="' + form.getvalue('game_play_type') + '", '
            play_type = form.getvalue('game_play_type')
    checks_incorrect = checks_incorrect + play_type_error

    form_running = 0
    if form.getvalue('game_running'):
        form_running = 1
    if running != form_running:
        update_sql = update_sql + 'running=' + str(form_running) + ', '
        running = form_running

    discord_role_error = ''
    if discord_role != form.getvalue('game_discord_role') and form.getvalue('game_discord_role'):
        if not (int(form.getvalue('game_discord_role')) in list(known_roles.values())):
            discord_role_error = 'Unknown Discord Role.\n'
        if discord_role_error == '':
            update_sql = update_sql + 'discord_role=' + str(form.getvalue('game_discord_role')) + ', '
            discord_role = int(form.getvalue('game_discord_role'))
    checks_incorrect = checks_incorrect + discord_role_error

    discord_channel_error = ''
    if discord_channel != form.getvalue('game_discord_channel') and form.getvalue('game_discord_channel'):
        if not (int(form.getvalue('game_discord_channel')) in list(known_channels.values())):
            discord_channel_error = 'Unknown Discord channel.\n'
        if discord_channel_error == '':
            update_sql = update_sql + 'discord_channel=' + str(form.getvalue('game_discord_channel')) + ', '
            discord_channel = int(form.getvalue('game_discord_channel'))
    checks_incorrect = checks_incorrect + discord_channel_error

    if update_sql != 'UPDATE game SET ':
        update_sql = update_sql[:-2] + ' WHERE `game`.`id`=' + str(game_id) + ';'
        database_handler.run(update_sql)

    for player_tag in all_player_tags:
        if form.getvalue('player_' + player_tag) and not (player_tag in player_tags):
            database_handler.link_game_player(game_id, database_handler.player_id_from_tag(player_tag))
            player_tags.append(player_tag)
        if not (form.getvalue('player_' + player_tag)) and player_tag in player_tags:
            database_handler.unlink_game_player(game_id, database_handler.player_id_from_tag(player_tag))
            player_tags.remove(player_tag)

current_role_name = list(known_roles.keys())[list(known_roles.values()).index(discord_role)]
current_channel_name = list(known_channels.keys())[list(known_channels.values()).index(discord_channel)]

del known_roles[current_role_name]
del known_channels[current_channel_name]

print('<body>')
page_constructor.menu(player_id)
print('<div id="content">')
print('<div id="edit-game">')
print('<h2>New Game</h2>')
print('<form id="update-game" name="edit_game" action="/games/edit" method="post">')
print('<div>Name: <input type="text" name="game_name" value="' + name + '"></div>')
print('<div>Type:')
print('<div id="edit-game-type-selection"><label class="edit-game-type"><input type="radio" name="game_play_type" value="longform" ')
if play_type == 'longform':
    print('checked')
print('><div class="edit-game-type-text">Longform</div></label>')
print('<label class="edit-game-type"><input type="radio" name="game_play_type" value="shortform" ')
if play_type == 'shortform':
    print('checked')
print('><div class="edit-game-type-text">Shortform</div></label>')
print('<label class="edit-game-type"><input type="radio" name="game_play_type" value="oneshot" ')
if play_type == 'oneshot':
    print('checked')
print('><div class="edit-game-type-text">Oneshot</div></label>')
print('<label class="edit-game-type"><input type="radio" name="game_play_type" value="unknown" ')
if play_type == 'unknown':
    print('checked')
print('><div class="edit-game-type-text">Unknown</div></label></div></div>')
print('<div id="edit-game-running">Running? <label id="edit-game-running"><input type="checkbox" name="game_running" value="game_running" ')
if running == 1:
    print('checked')
print('><div id="edit-game-running-yay">YAY</div><div id="edit-game-running-nay">NAY</div></label></div>')

print('<div>Discord:')
print('<div id="edit-game-discord-id">')
print('<div>Role: <select name="game_discord_role">')
print('<option value=' + str(discord_role) + '>' + current_role_name + '</option>')
for role in known_roles.keys():
    print('<option value=' + str(known_roles[role]) + '>' + role + '</option>')
print('</select></div>')
print('<div>Channel: <select name="game_discord_channel">')
print('<option value=' + str(discord_channel) + '>' + current_channel_name + '</option>')
for channel in known_channels.keys():
    print('<option value=' + str(known_channels[channel]) + '>' + channel + '</option>')
print('</select></div>')
print('</div></div>')

if form.getvalue("update_form") != None:
    print('<p id="edit-game-error">' + checks_incorrect + '</p>')
    
print('<div id="game-edit-player-container">Players: ')

for tag in all_player_tags:
    print('<label class="game-edit-player">')
    print('<input type="checkbox" name="player_' + tag + '" value="' + tag + '" ')
    if tag in player_tags:
        print('checked')
    print('><div class="game-edit-player-text">' + tag + '</div>')
    print('</label>')

print('</div>')
print('<input type="checkbox" hidden id="update_form" name="update_form" value="update_form" checked>')
print('<input type="checkbox" hidden id="edit_game" name="edit_game" value="' + str(game_id) + '" checked>')
print('</form>')
print('<h2>Edit Game</h2>')
print('</div>')
print('<input type="submit" form="update-game" value="Save">')
print('</div>')
print('</body>')