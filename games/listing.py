#!/usr/bin/python3
import sys
sys.path.insert(1, '../_lib')
import page_constructor
import access_management
import database_handler

page_constructor.headers()
player_id = access_management.check_session()
page_constructor.head('Rook | Games')

print('<body>')
page_constructor.menu(player_id)
print('<div id="content">')


print('<ul id="game-list">')

all_games = database_handler.run('SELECT * FROM game;').fetchall()
games_playing_in = database_handler.run('SELECT game_id FROM `game_player` WHERE player_id=' + str(player_id) + ';').fetchall()

owned_games = []
playing_games = []
others_games = []
inactive_owned_games = []
inactive_playing_games = []
inactive_others_games = []

for game in all_games:
    if game[7] == player_id:
        if game[4] == 1:
            owned_games.append(game)
        else:
            inactive_owned_games.append(game)
    elif game[0] in games_playing_in:
        if game[4] == 1:
            playing_games.append(game)
        else:
            inactive_playing_games.append(game)
    else:
        if game[4] == 1:
            others_games.append(game)
        else:
            inactive_others_games.append(game)

sorted_games = owned_games + playing_games + others_games + inactive_owned_games + inactive_playing_games + inactive_others_games

for game in sorted_games:
    if game[4] == 1:
        print('<li class="game-listing">')
    else:
        print('<li class="game-listing-inactive">')
    if game[2]:
        print('<img src="' + game[2] + '" />')
    else:
        print('<img src="/styling/users/icons/default.png" />')
    print('<div class="game-details">')
    print('<div class="game-heading">')
    print('<h2>' + game[1] + '</h2>')
    print('<h3>' + game[3] + '</h3>')
    print('</div>')
    if game[7] == player_id:
        print('<form name="new_game" action="/games/edit" method="post">')
        print('<input type="checkbox" hidden id="edit_game" name="edit_game" value="' + str(game[0]) + '" checked>')
        print('<input class="edit-game" type="submit" value="Edit">')
        print('</form>')
    else:
        print('<h4 class="game-owner">' + database_handler.player_record(game[7])[3] + '</h4>')
    print('<ul class="game-players">')
    for player in database_handler.get_players(game[0]):
        if player[0] != player_id:
            print('<li>')
            print(database_handler.player_record(player[0])[3])
            print('</li>')
    print('</ul>')
    print('</div>')
    print('</li>')
print('</ul>')
print('<form name="new_game" action="/games/new" method="post">')
print('<input type="submit" value="New">')
print('</div>')
print('</body>')