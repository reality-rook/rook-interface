#!/usr/bin/python3
import sys
sys.path.insert(1, '../_lib')
import page_constructor
import access_management
import discord_actions
import database_handler
import string

page_constructor.headers()
player_id = access_management.check_session()
page_constructor.head('Rook | New Game')

import cgi
form = cgi.FieldStorage()
name = form.getvalue('game_name')
play_type = form.getvalue('game_play_type')
running = form.getvalue('game_running')
make_role  = form.getvalue('game_make_role')
make_channel = form.getvalue('game_make_channel')

if not name:
    name = ''
if not play_type:
    play_type = 'unknown'

checks_cleared = True
checks_incorrect = ""
whitelist = set(string.ascii_lowercase + string.digits + string.ascii_uppercase + ' _-')
if not any(c not in whitelist for c in name):
    if database_handler.verify_record('game', 'name', name):
        checks_cleared = False
        checks_incorrect = 'That game has already been added to Rook.\n'
else:
    checks_cleared = False
    checks_incorrect = checks_incorrect + 'Name must be only letters and numbers.\n'
if name == '':
    checks_cleared = False
    checks_incorrect = 'The game must have a name.\n'
if len(name) > 32:
    checks_cleared = False
    checks_incorrect = 'The game name must be less than 33 characters.\n'

if checks_cleared:
    discord_role = 0
    discord_channel = 0

    discord_action_dict = {}
    discord_ids = {}
    if make_role:
        discord_action_dict["make_role"] = [name]
    if make_channel:
        discord_action_dict["make_channel"] = [name]
    if discord_action_dict != {}:
        discord_ids = discord_actions.run_actions(discord_action_dict)
        if make_role:
            discord_role = discord_ids['make_role']
        if make_channel:
            discord_channel = discord_ids['make_channel']
    if running:
        running = 1
    else:
        running = 0
    database_handler.add_game(name, play_type, running, discord_role, discord_channel, player_id)
    game_id = database_handler.game_id_from_name(name)
    database_handler.link_game_player(game_id, player_id)
    print('\n\n<meta http-equiv="refresh" content="0; URL=/games/listing"/>')

print('<body>')
page_constructor.menu(player_id)
print('<div id="content">')
print('<div id="edit-game">')
print('<h2>New Game</h2>')
print('<form id="new-game" name="new_game" action="/games/new" method="post">')
print('<div>Name: <input type="text" name="game_name" value=' + name + '></div>')
print('<div>Type:')
print('<div id="edit-game-type-selection"><label class="edit-game-type"><input type="radio" name="game_play_type" value="longform"><div class="edit-game-type-text">Longform</div></label>')
print('<label class="edit-game-type"><input type="radio" name="game_play_type" value="shortform"><div class="edit-game-type-text">Shortform</div></label>')
print('<label class="edit-game-type"><input type="radio" name="game_play_type" value="oneshot"><div class="edit-game-type-text">Oneshot</div></label>')
print('<label class="edit-game-type"><input type="radio" name="game_play_type" value="unknown"><div class="edit-game-type-text">Unknown</div></label></div></div>')
print('<div id="edit-game-running">Running? <label id="edit-game-running"><input type="checkbox" name="game_running" value="game_running"><div id="edit-game-running-yay">YAY</div><div id="edit-game-running-nay">NAY</div></label></div>')
print('<div>Discord Shortcuts:')
print('<div id="edit-game-discord-selection">')
print('<label class="edit-game-discord"><input type="checkbox" name="game_make_role" value="game_make_role"><div class="edit-game-discord-text">Make Role</div></label>')
print('<label class="edit-game-discord"><input type="checkbox" name="game_make_channel" value="game_make_channel"><div class="edit-game-discord-text">Make Channel</div></label>')
print('</div></div>')
print('<input type="checkbox" hidden id="update_form" name="update_form" value="update_form" checked>')
print('</form>')
if form.getvalue("update_form") != None:
    print('<p id="edit-game-error">' + checks_incorrect + '</p>')
print('<h2>New Game</h2>')
print('</div>')
print('<input type="submit" form="new-game" value="Create">')
print('</div>')
print('</body>')