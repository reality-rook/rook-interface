#!/usr/bin/python3
import sys
sys.path.insert(1, './_lib')
import page_constructor
import database_handler
import access_management

page_constructor.headers()
player_id = access_management.check_session()
page_constructor.head('Rook | Home')

print('<body>')
page_constructor.menu(player_id)
print('<div id="content">')

print('<div id="game-order">')
print('<h2>Previous Game Log</h2>')
for record_id in database_handler.run('SELECT id,game_id FROM game_order ORDER BY `game_order`.`time` DESC;').fetchall():
    record = database_handler.run('SELECT * FROM game WHERE id=' + str(record_id[1]) + ';').fetchone()
    print('<div class="game-order-listing">')
    print(record[1])
    print('</div>')
print('<h2>Previous Game Log</h2>')
print('</div>')

print('</div>')
print('</body>')