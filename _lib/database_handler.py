import os
import json
import mysql.connector
import string

database = mysql.connector
config = {}
to_root_path = ''
for level in range(0,4):
    if '_config' in os.listdir(to_root_path + './'):
        break
    else:
        to_root_path = to_root_path + '../'
with open(to_root_path + './_config/database.conf', 'r') as conf_file:
    config = json.loads(conf_file.read())
database = database.connect(
        host = config['host'],
        user = config['user'],
        password = config['password'],
        database = config['database']
    )



def run(command):
    database_cursor = database.cursor(buffered=True)
    database_cursor.execute(command)
    database.commit()
    return database_cursor

def get_records(table, column, value):
    column_type_sql = 'SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = "' + table + '" AND COLUMN_NAME = "' + column + '";'
    database_cursor = database.cursor(buffered=True)
    database_cursor.execute(column_type_sql)
    column_type = database_cursor.fetchone()[0].decode('ASCII')
    sql = 'SELECT * FROM ' + table + ' WHERE ' + column + ' = '
    if 'text' in column_type or 'char' in column_type:
        sql = sql + '"'
    sql = sql + value
    if 'text' in column_type or 'char' in column_type:
        sql = sql + '"'
    sql = sql + ';'
    database_cursor.execute(sql)
    return database_cursor.fetchall()

def verify_record(table, column, value):
    try:
        sql = 'SELECT id FROM ' + table + ' WHERE ' + column + ' = "' + value + '";'

        if run(sql).rowcount > 0:
            return True
        else:
            return False
    except mysql.connector.errors.ProgrammingError:
        return False

def player_id_from_tag(tag):
    sql = 'SELECT id FROM player WHERE tag = "' + tag + '";'
    return run(sql).fetchone()[0]

def player_record(player_id):
    sql = 'SELECT * FROM player WHERE id=' + str(player_id) + ';'
    return run(sql).fetchone()

def add_player(tag, password, name, discord_id):
    sql = 'INSERT INTO `player` (`id`, `tag`, `password`, `name`, `icon_url`, `discord_id`) VALUES ('
    sql = sql + 'NULL, "'
    sql = sql + tag + '", "'
    sql = sql + password + '", "'
    sql = sql + name + '", '
    sql = sql + 'NULL, '
    sql = sql + str(discord_id)
    sql = sql + ');'
    run(sql)

def get_hashed_password(player_id):
    sql = 'SELECT password FROM player WHERE id=' + str(player_id) +';'
    return run(sql).fetchone()[0]

def get_player_days(player_id):
    sql = 'SELECT id, day FROM day WHERE player_id=' + str(player_id) + ';'
    return run(sql).fetchall()

def get_player_dayhours(player_id):
    dayhours = []
    sql = 'SELECT id, day FROM day WHERE player_id=' + str(player_id) +';'
    day_rows = run(sql).fetchall()
    for day_row in day_rows:
        sql = 'SELECT id, hour FROM hour WHERE day_id=' + str(day_row[0]) + ';'
        hour_rows = run(sql).fetchall()
        for hour_row in hour_rows:
            dayhours.append(day_row[1] + str(hour_row[1]))
    return dayhours

def add_player_dayhour(player_id, dayhour):
    if dayhour in get_player_dayhours(player_id):
        return
    day = dayhour[:2]
    player_days = get_player_days(player_id)
    day_exists = False
    for index in range(0, len(player_days)):
        if player_days[index][1] == day:
            day_exists = True
    if not day_exists:
        sql = 'INSERT INTO `day` (`id`, `player_id`, `day`) VALUES ('
        sql = sql + 'NULL, '
        sql = sql + str(player_id) + ', "'
        sql = sql + day
        sql = sql + '");'
        run(sql)
        player_days = get_player_days(player_id)
    for index in range(0, len(player_days)):
        if player_days[index][1] == day:
            sql = 'INSERT INTO `hour` (`id`, `day_id`, `hour`) VALUES ('
            sql = sql + 'NULL, '
            sql = sql + str(player_days[index][0]) + ', '
            sql = sql + dayhour[2:]
            sql = sql + ');'
            run(sql)
            return
    

def remove_player_dayhour(player_id, dayhour):
    if not (dayhour in get_player_dayhours(player_id)):
        return
    day = dayhour[:2]
    player_days = get_player_days(player_id)
    for player_day in player_days:
        if player_day[1] == day:
            sql = 'DELETE FROM hour WHERE `hour`.`day_id`=' + str(player_day[0])
            sql = sql + ' AND `hour`=' + dayhour[2:] + ';'
            run(sql)
            sql = 'SELECT id FROM hour WHERE day_id=' + str(player_day[0]) + ';'
            hours_under_day = run(sql).fetchall()
            if len(hours_under_day) < 1:
                sql = sql = 'DELETE FROM day WHERE `day`.`id`=' + str(player_day[0]) + ';'
                run(sql)

def add_game(name, play_type, running, discord_role, discord_channel, owner_id):
    sql = 'INSERT INTO `game` (`id`, `name`, `icon_url`, `type`, `running`, `discord_role`, `discord_channel`, `owner_id`) VALUES ('
    sql = sql + 'NULL, "'
    sql = sql + name + '", '
    sql = sql + 'NULL, "'
    sql = sql + play_type + '", '
    sql = sql + str(running) + ', '
    sql = sql + str(discord_role) + ', '
    sql = sql + str(discord_channel) + ', '
    sql = sql + str(owner_id) + ');'
    run(sql)

def get_running_games():
    sql = 'SELECT * FROM game WHERE running=1;'
    return run(sql).fetchall()

def game_id_from_name(name):
    sql = 'SELECT id FROM game WHERE name="' + name + '";'
    return run(sql).fetchone()[0]

def get_players(game_id):
    sql = 'SELECT player_id FROM game_player WHERE game_id=' + str(game_id) + ';'
    return run(sql).fetchall()

def link_game_player(game_id, player_id):
    sql = 'INSERT INTO `game_player` (`id`, `game_id`, `player_id`) VALUES ('
    sql = sql + 'NULL, "' + str(game_id) + '", "' + str(player_id) + '");'
    run(sql)

def unlink_game_player(game_id, player_id):
    sql = 'DELETE FROM game_player WHERE `game_player`.`game_id` = ' + str(game_id)
    sql = sql + ' AND `game_player`.`player_id`=' + str(player_id) + ';'
    run(sql)

def add_session(player, token):
    sql = 'INSERT INTO `session` (`id`, `player_id`, `token`, `age`, `last_used`) VALUES ('
    sql = sql + 'NULL, '
    sql = sql + str(player) + ', "'
    sql = sql + token + '", '
    sql = sql + 'CURRENT_TIMESTAMP, '
    sql = sql + 'CURRENT_TIMESTAMP'
    sql = sql + ');'
    run(sql)

def find_session(session_token):
    character_whitelist = set(string.ascii_uppercase + string.digits + string.ascii_lowercase)
    session_token = ''.join(filter(character_whitelist.__contains__, session_token))
    sql = 'SELECT player_id FROM session WHERE token="' + session_token +'";'
    try:
        return run(sql).fetchone()[0]
    except:
        return False


def remove_session(session_token):
    sql = 'DELETE FROM session WHERE `session`.`token` = "' + session_token + '";'
    run(sql)