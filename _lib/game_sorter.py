import sys
sys.path.insert(1, '../_lib')
import database_handler
import random
import string
import os
from math import sqrt

def update_weights():
    games = database_handler.get_running_games()
    game_player_dict = {}
    for game in games:
        players = []
        for player_record in database_handler.get_players(game[0]):
            players.append(player_record[0])
        game_player_dict[game[0]] = players
    game_order = database_handler.run('SELECT id FROM game_order ORDER BY `game_order`.`time` DESC;').fetchall()
    previous_games = []
    for game in game_order:
        previous_games.append(game[0])
    weights = calculate_game_weights(game_player_dict, previous_games)
    database_handler.run('DELETE FROM priority_weight;')
    for weight in weights.keys():
        database_handler.run('INSERT INTO `priority_weight` (`id`, `game_id`, `weight`) VALUES (NULL, ' + str(weight) + ', ' + str(weights[weight]) + ');')

def sorted_games():
    game_weights = database_handler.run('SELECT * FROM priority_weight ORDER BY `priority_weight`.`weight` ASC;').fetchall()
    games_in_order =[]
    for game in game_weights:
        games_in_order.append(database_handler.run('SELECT * FROM game WHERE id=' + str(game[1]) + ';').fetchone())
    return games_in_order

def sorted_updated_games():
    update_weights()
    return sorted_games()

def calculate_game_weights(running_games, previous_games):
    # running games - {"game_name": ["player", "player"]}

    # Stores the end scores as they are manipulated by the following loops. LOWER IS BETTER
    game_weights = {}

    # Give the base weight of a game, using an inverted exponential function to give older games a larger advantage.
    for cycle_index in range(len(previous_games)):
        current_game = previous_games[cycle_index]
        if current_game in running_games:
            current_weight = 2 ** ( -1 * ( cycle_index - 5 ))
            if current_game in game_weights:
                game_weights[current_game] += current_weight
            else:
                game_weights[current_game] = current_weight

    for game_key in running_games.keys():
        if not (game_key in game_weights.keys()):
            game_weights[game_key] = 1

    # Prepare frequency, which is meant to show how much a specific player has been playing recently.
    player_frequency = {}

    # Populate a dict with all known players present in the list of running games.
    #This needs to happen to make sure players absent from the cycle dataset get a value.
    for game_key in running_games.keys():
        for player in running_games[game_key]:
            if player not in player_frequency:
                player_frequency[player] = 0

    # Use a toned down version of the game weight to make player positioning in the recent cycles tracked.
    for game_key in game_weights.keys():
        for player in running_games[game_key]:
            if player in player_frequency:
                player_frequency[player] += sqrt(game_weights[game_key])

    # Modify game weights to take player frequency in to account. Also weight for games with more players.
    for game_key in game_weights.keys():
        total_player_frequency = 0
        for player in running_games[game_key]:
            total_player_frequency += player_frequency[player]
        game_weights[game_key] = game_weights[game_key] * (total_player_frequency / len(running_games[game_key]))
        game_weights[game_key] = game_weights[game_key] * ( 1 + (len(running_games[game_key]) / 10 ))

    return game_weights

def sort_by_weights(weight_dict):
    sorted_keys = []
    sorted_weights = [0]
    for weight_key in weight_dict.keys():
        current_weight = weight_dict[weight_key]
        for weight_index in range(len(sorted_weights)):
            if current_weight < sorted_weights[weight_index] or weight_index == len(sorted_weights) - 1:
                sorted_keys.insert(weight_index, weight_key)
                sorted_weights.insert(weight_index, current_weight)
                break

    return sorted_keys
