import os
import json
import string
import asyncio
import discord

config = {}
to_root_path = ''
for level in range(0,4):
    if '_config' in os.listdir(to_root_path + './'):
        break
    else:
        to_root_path = to_root_path + '../'
with open(to_root_path + './_config/discord.conf', 'r') as conf_file:
    config = json.loads(conf_file.read())

to_return = {}
action_dict = {}

name = "Placeholder"

intents = discord.Intents.default()

class tempClient(discord.Client):
    async def on_ready(self):
        try:
            global config
            global to_return
            global action_dict
            guild = await self.fetch_guild(config["guild"])
            category = await self.fetch_channel(config['category'])

            for action in action_dict.keys():
                filtered_action = action.split('0')[0]
                action_params = action_dict[action]
                if filtered_action == "make_role":
                    role = await guild.create_role(name=action_params[0])
                    to_return[action] = role.id 

                if filtered_action == "make_channel":
                    action_params[0] = action_params[0].replace(' ', '-')
                    action_params[0] = action_params[0].lower()
                    channel = await category.create_text_channel(action_params[0])
                    to_return[action] = channel.id
                
                if filtered_action == "get_roles":
                    roles = await guild.fetch_roles()
                    returning_roles = {}
                    for role in roles:
                        returning_roles[role.name] = role.id
                    to_return[action] = returning_roles

                if filtered_action == "get_channels":
                    channels = category.text_channels
                    returning_channels = {}
                    for channel in channels:
                        returning_channels[channel.name] = channel.id
                    to_return[action] = returning_channels

                if filtered_action == "give_role":
                    member = await guild.fetch_member(action_params[0])
                    role = await guild.get_role(action_params[1])
                    member.add_roles(role)
                    to_return[action] = ''

                if filtered_action == "take_role":
                    member = await guild.fetch_member(action_params[0])
                    role = await guild.get_role(action_params[1])
                    member.remove_roles(role)
                    to_return[action] = ''

            await self.close()
        except Exception as e:
            print(e)
            await self.close()

def run_actions(new_action_dict):
    global config
    global to_return
    global action_dict
    action_dict = new_action_dict

    client = tempClient(intents=intents)
    client.run(config['token'])

    return to_return
