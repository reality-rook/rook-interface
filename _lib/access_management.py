import sys
sys.path.insert(1, '../_lib')
import database_handler
import page_constructor
import random
import string
import os

def start_session(player):
    new_session_token = ''.join(random.choices(string.ascii_uppercase + string.digits + string.ascii_lowercase, k=24))
    database_handler.add_session(player, new_session_token)
    return new_session_token

def send_session(session_token):
    print('Set-Cookie: session=' + session_token + ';path=/')

def check_session(redirect=True):
    session_token = ''
    try:
        for cookie in os.environ['HTTP_COOKIE'].split(';'):
            if cookie.startswith('session'):
               session_token = cookie.split('=')[1]
    except KeyError:
        pass
    if session_token == '':
        if redirect:
            print('\n\n<meta http-equiv="refresh" content="0; URL=/auth/login"/>')
        return False
    player_id = database_handler.find_session(session_token)
    if player_id:
        return player_id
    else:
        if redirect:
            print('\n\n<meta http-equiv="refresh" content="0; URL=/auth/login"/>')
        return False

def end_session(session_token):
    database_handler.remove_session(session_token)