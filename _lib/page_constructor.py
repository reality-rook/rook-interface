import sys
sys.path.insert(1, '../_lib')
import database_handler
import os


def headers():
    print("Content-type: text/html")
    print('Cache-Control: no-cache, no-store, must-revalidate')
    print('Pragma: no-cache')
    print('Expires: 0')


def head(title):
    print("\n\n<html>")
    print('<head>')
    print('<meta charset="utf-8">')
    print('<title>' + title + '</title>')
    print('<link href="/styling/css/main.css?version=1" rel="stylesheet" type="text/css"/>')
    print('<script>')
    print('function open_menu(){document.getElementById("menu").style.display="inline-block";')
    print('document.getElementById("menu-button").style.display="none";')
    print('document.getElementById("content").style.display="none"};')
    print('function close_menu(){document.getElementById("menu").style.display="none";')
    print('document.getElementById("menu-button").style.display="block";')
    print('document.getElementById("content").style.display="block"};')
    print('</script>')
    print('</head>')

def menu(player_id):
    print('<div id="menu"><div id="menu-flex">')
    print('<div id="profile">')
    
    player_record = database_handler.player_record(player_id)
    if player_record[4]:
        print('<img src="' + player_record[4] + '" />')
    else:
        print('<img src="/styling/users/icons/default.png" />')
    print('<h1>' + player_record[3] + '</h1>')
    print('<h2>' + player_record[1] + '</h2>')
    print('</div>')
    print('<div id="navigation">')
    print('<ul>')
    print('<li><a href="/schedule/edit" class="menu-button"> Schedule </a></li>')
    print('<li><a href="/games/listing" class="menu-button"> Games </a></li>')
    print('<li><a href="/rotation/overview" class="menu-button"> Rotation </a></li>')
    print('</ul>')
    print('</div>')
    print('<div id="bottom-row">')
    print('<a id="logout" href="/auth/logout" class="menu-button"> Logout </a>')
    print('<a id="dashboard" href="/" class="menu-button"> Dashboard </a>')
    print('<button id="close-menu-button" class="menu-button" onclick="close_menu()"><<</button>')
    print('</div>')
    print('</div></div>')

    print('<div id="menu-button">')
    print('<button id="open-menu-button" onclick="open_menu()">>></button>')
    print('</div>')