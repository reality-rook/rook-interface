#!/usr/bin/python3
import sys
sys.path.insert(1, '../_lib')
import page_constructor
import access_management
import database_handler
import game_sorter
import random

page_constructor.headers()
player_id = access_management.check_session()
page_constructor.head('Rook | Rotation')

games = game_sorter.sorted_updated_games()

start_hour = 2
schedule = {
    'Mo': [],
    'Tu': [],
    'We': [],
    'Th': [],
    'Fr': [],
    'Sa': [],
    'Su': []
}

daycode_converter = {
    'Mo': 'Monday',
    'Tu': 'Tuesday',
    'We': 'Wednesday',
    'Th': 'Thursday',
    'Fr': 'Friday',
    'Sa': 'Saturday',
    'Su': 'Sunday'
}

colors = [
    '120, 255, 203',
    '245, 148, 113',
    '233, 156, 255',
    '255, 172, 234',
    '171, 71, 188',
    '220, 20, 60',
    '133, 59, 164',
    '113, 38, 159',
    '6, 6, 6'
]

color_codes = {}

for day in schedule.keys():
    for hour in range(start_hour,12):
        schedule[day].append([])

for game in games:
    player_lists = database_handler.get_players(game[0])
    player_ids = []
    for player in player_lists:
        player_ids.append(player[0])
    dayhours_counted = {}
    player_count = len(player_ids)
    for player in player_ids:
        player_dayhours = database_handler.get_player_dayhours(player)
        for dayhour in player_dayhours:
            if dayhour in dayhours_counted.keys():
                dayhours_counted[dayhour] = dayhours_counted[dayhour] + 1
            else:
                dayhours_counted[dayhour] = 1
            if dayhours_counted[dayhour] >= player_count:
                schedule[dayhour[:2]][int(dayhour[2:]) - start_hour].append(game[0])
                if not (game[0] in color_codes):
                    random.seed(game[1])
                    selected = random.randint(0,len(colors) - 1)
                    color_codes[game[0]] = [game[1], colors[selected]]
                    colors.pop(selected)

print('<body>')
page_constructor.menu(player_id)
print('<div id="content">')
print('<div id="legend">')
for game in color_codes.keys():
    print('<div class="legend-color"><div class="color-preview" style="background-color: rgb(' + color_codes[game][1] + ');"></div><div class="color-label" style="border-color: rgb(' + color_codes[game][1] + ');">' + color_codes[game][0] + '</div></div>')
print('</div>')
print('<div id="calendar">')
print('<div class="day">')
print('<h3 class="day-label"></h3>')
for hour in range(0, len(schedule['Mo'])):
    print('<div class="number-cell">' + str(hour + start_hour) + '</div>')
print('</div>')
for day in schedule.keys():
    print('<div class="day">')
    print('<h3 class="day-label">' + daycode_converter[day] + '</h3>')
    for hour in schedule[day]:
        print('<div class="hour-cell">')
        for game in hour:
            print('<div class="game-cell" style="background-color: rgb(' + color_codes[game][1] + ');"></div>')
        print('</div>')
    print('</div>')
print('</div>')
print('</div>')
print('</body>')