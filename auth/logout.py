#!/usr/bin/python3
import sys
sys.path.insert(1, '../_lib')
import page_constructor
import access_management
import os

page_constructor.headers()

session_token = ''
try:
    for cookie in os.environ['HTTP_COOKIE'].split(';'):
        if cookie.startswith('session'):
            session_token = cookie.split('=')[1]
except KeyError:
    pass

print('Set-Cookie: session=0;path=/')


if session_token != '':
    access_management.end_session(session_token)

page_constructor.head("Rook | Logout")

print('\n\n<meta http-equiv="refresh" content="0; URL=/auth/login"/>')