#!/usr/bin/python3
import sys
sys.path.insert(1, '../_lib')
import page_constructor
import database_handler
import access_management
import bcrypt

page_constructor.headers()

if access_management.check_session(redirect=False):
    page_constructor.head("Rook | Register")
    print('\n\n<meta http-equiv="refresh" content="0; URL=/"/>')

import cgi
form = cgi.FieldStorage()
code = form.getvalue('reg_code')
name = form.getvalue('reg_name')
tag = form.getvalue('reg_tag')
password = form.getvalue('reg_password')

if not code:
    code = ""
if not name:
    name = ""
if not tag:
    tag = ""
if not password:
    password = ""


checks_cleared = True
checks_incorrect = ""
if code.isalnum():
    if not database_handler.verify_record('auth_code', 'code', code):
        checks_cleared = False
        checks_incorrect = checks_incorrect + "Registration code not found. If the code was typed correctly, try asking Rook for another in Discord.<br>"
if name == "":
    checks_cleared = False
    checks_incorrect = checks_incorrect + "No name specified! People need to know who you are.<br>"
if len(name) > 99:
    checks_cleared = False
    checks_incorrect = checks_incorrect + "Desired name is too long! Keep the number of characters below triple digits.<br>"
if not name.isalnum():
    checks_cleared = False
    checks_incorrect = checks_incorrect + "Name may only contain letters of the alphabet, and numbers.<br>"
if tag == "":
    checks_cleared = False
    checks_incorrect = checks_incorrect + "No tag specified! People need to know who you are.<br>"
if len(tag) >= 16:
    checks_cleared = False
    checks_incorrect = checks_incorrect + "User tags must be no more than 16 characters.<br>"
if not tag.isalnum():
    checks_cleared = False
    checks_incorrect = checks_incorrect + "Tag may only contain letters of the alphabet, and numbers.<br>"
if checks_cleared:
    if database_handler.verify_record('player', 'tag', tag):
        checks_cleared = False
        checks_incorrect = checks_incorrect + "Another player has already taken that tag. Please choose another.<br>"
if len(password) < 6:
    checks_cleared = False
    checks_incorrect = checks_incorrect + "Password needs to be at least 6 characters.<br>"
if len(password) > 32:
    checks_cleared = False
    checks_incorrect = checks_incorrect + "Password needs to be less than 33 characters.<br>"
if password.isalnum():
    checks_cleared = False
    checks_incorrect = checks_incorrect + "Passwords should contain at least one special character.<br>"



if checks_cleared:
    hashed_password = bcrypt.hashpw(password, bcrypt.gensalt(15))
    discord_id = database_handler.get_records('auth_code', 'code', code)[0][3]
    database_handler.add_player(tag, hashed_password, name, discord_id)
    access_management.send_session(access_management.start_session(database_handler.player_id_from_tag(tag)))
    database_handler.run('DELETE FROM auth_code WHERE `auth_code`.`code` = "' + code + '";')
    page_constructor.head("Rook | Register")
    print('\n\n<meta http-equiv="refresh" content="0; URL=/"/>')

page_constructor.head('Rook | Register')

print('<body>')
print('<div id="auth-container">')
print('<h2>Register</h2>')
print('<form name="search" action="/auth/register" method="post">')
print('<div>Registration Code: <input type="text" name="reg_code" value=' + code + '></div>')
print('<div>Name: <input type="text" name="reg_name" value=' + name + '></div>')
print('<div>Tag: <input type="text" name="reg_tag" value=' + tag + '></div>')
print('<div>Password: <input type="password" name="reg_password"></div>')
print('<input type="checkbox" hidden id="update_form" name="update_form" value="update_form" checked>')
print('<input class="auth-submit" type="submit" value="Register">')
print('</form>')
if form.getvalue("update_form") != None:
    print('<p id="auth-error">' + checks_incorrect + '</p>')
print('<h2>Register</h2>')
print('</div>')
print('</body>')