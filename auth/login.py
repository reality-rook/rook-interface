#!/usr/bin/python3
import sys
sys.path.insert(1, '../_lib')
import page_constructor
import database_handler
import access_management
import bcrypt

page_constructor.headers()

if access_management.check_session(redirect=False):
    page_constructor.head("Rook | Log In")
    print('\n\n<meta http-equiv="refresh" content="0; URL=/"/>')


import bcrypt

import cgi
form = cgi.FieldStorage()
tag = form.getvalue('lin_tag')
password = form.getvalue('lin_password')


if not tag:
    tag = ""
if not password:
    password = ""


checks_cleared = True
checks_incorrect = ""
if tag == "":
    checks_cleared = False
if len(tag) >= 16:
    checks_cleared = False
if not tag.isalnum():
    checks_cleared = False
if checks_cleared:
    if not database_handler.verify_record('player', 'tag', tag):
        checks_cleared = False
if len(password) < 6:
    checks_cleared = False
if len(password) > 32:
    checks_cleared = False
if password.isalnum():
    checks_cleared = False

try:
    player_id = database_handler.player_id_from_tag(tag)
    stored_password = database_handler.get_hashed_password(player_id)
    if not bcrypt.checkpw(password, stored_password):
        checks_cleared = False
except:
    checks_cleared = False

if checks_cleared:
    access_management.send_session(access_management.start_session(database_handler.player_id_from_tag(tag)))
    page_constructor.head("Rook | Log In")
    print('\n\n<meta http-equiv="refresh" content="0; URL=/"/>')


page_constructor.head('Rook | Log In')

print('<body>')
print('<div id="auth-container">')
print('<h2>Login</h2>')
print('<form name="login" action="/auth/login" method="post">')
print('<div>Tag: <input type="text" name="lin_tag" value=' + tag + '></div>')
print('<div>Password: <input type="password" name="lin_password"></div>')
print('<input type="checkbox" hidden id="update_form" name="update_form" value="update_form" checked>')
print('<input class="auth-submit" type="submit" value="Login">')
print('</form>')
if not checks_cleared and tag is not None and password is not None and form.getvalue("update_form") != None:
    print('<p id="auth-error">No account with that Tag and Password found. Did you type them correctly?</p>')
print('<h2>Login</h2>')
print('</div>')
print('</body>')