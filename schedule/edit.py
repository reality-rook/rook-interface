#!/usr/bin/python3
import sys
sys.path.insert(1, '../_lib')
import page_constructor
import access_management
import database_handler

page_constructor.headers()
player_id = access_management.check_session()
page_constructor.head('Rook | Schedule')

import cgi
form = cgi.FieldStorage()

start_hour = 2
schedule = {
    'Mo': [],
    'Tu': [],
    'We': [],
    'Th': [],
    'Fr': [],
    'Sa': [],
    'Su': []
}

daycode_converter = {
    'Mo': 'Monday',
    'Tu': 'Tuesday',
    'We': 'Wednesday',
    'Th': 'Thursday',
    'Fr': 'Friday',
    'Sa': 'Saturday',
    'Su': 'Sunday'
}

day_suit = {
    'Mo': 'black',
    'Tu': 'green',
    'We': 'black',
    'Th': 'green',
    'Fr': 'red',
    'Sa': 'yellow',
    'Su': 'yellow'
}

for day in schedule.keys():
    for hour in range(start_hour,12):
        schedule[day].append(False)


database_dayhours = database_handler.get_player_dayhours(player_id)
for dayhour in database_dayhours:
    schedule[dayhour[:2]][int(dayhour[2:]) - start_hour] = True

if form.getvalue("update_form") != None:
    for day in schedule.keys():
        for hour_index in range(0, len(schedule[day])):
            target_hour = start_hour + hour_index
            if form.getvalue(day + str(target_hour)) != None and schedule[day][hour_index] == False:
                database_handler.add_player_dayhour(player_id, day + str(target_hour))
                schedule[day][hour_index] = True
            elif form.getvalue(day + str(target_hour)) == None and schedule[day][hour_index] == True:
                database_handler.remove_player_dayhour(player_id, day + str(target_hour))
                schedule[day][hour_index] = False

print('<body>')
page_constructor.menu(player_id)
print('<div id="content">')

print('<div id="schedule-edit">')
print('<form name="schedule" action="/schedule/edit" method="post">')
schedule_string = ''
for day in schedule.keys():
    schedule_string = schedule_string + '<div class="schedule-edit-day" style="--day-color: var(--rook-' + day_suit[day] + ');"><h2 class="schedule-day-label">' + daycode_converter[day] + '</h2><div class="checkbox-row">'
    for hour_index in range(0, len(schedule[day])):
        target_hour = start_hour + hour_index
        schedule_string = schedule_string + '<label class="checkbox-label"><input type="checkbox" id="' + day + str(target_hour) + '" name="' + day + str(target_hour) + '" value=true'
        if schedule[day][hour_index]:
            schedule_string = schedule_string + ' checked'
        schedule_string = schedule_string + '><div class="label-text">' + str(target_hour) + '</div></label>'
    schedule_string = schedule_string + '</div>'
    schedule_string = schedule_string + '<h2 class="schedule-day-label">' + daycode_converter[day] + '</h2></div>'
print(schedule_string)
print('<input type="checkbox" hidden id="update_form" name="update_form" value="update_form" checked>')
print('<input type="submit" value="Save">')
print('</div>')

print('</div>')
print('</body>')