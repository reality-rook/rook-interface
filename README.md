# Web Interface
This is the Reality Rook webpage that can be used to manage games, players, and the schedule. This is a requirement of every Rook instance that does not wish to reimpliment its features in other locations.

## Setup
This is currently a direct set of Python files that can be served by an apache webserver. Currently, the only known Rook instance is running on a shared server where apache comes preconfiguredd to be able to run Python files. If your enviroment supports it, setting this up requires cloning this repository, filling in the _config with your instance details, and setup an integration to make an account.
